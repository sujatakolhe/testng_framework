package testNG_Validation;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPostRequest_class;
import Repository_package.RPost_Requset;
import TestDelivery_package.TestCaseDelivery_class;
import io.restassured.path.json.JsonPath;

public class TestNG_Data_Driven extends Common_method_handle_API {
	static File log_dir;
	static String post_requestBody;
	static String post_endpoint;
	static String post_responsebody;
	
	@DataProvider()
	public Object[][] post_requestBody()
	  {
		return new Object [] []
		{
			{"morpheus", "leader"},
			{"Sujata","QA"},
			{"Pravin","Manager"}
			
		};
	  }
		
		

	@BeforeTest
	public static void Test_setup() throws IOException {
		log_dir = Dir_Handle.create_DirLog("TestCaseDelivery_class");
		
		post_endpoint = EPostRequest_class.EPostRequest_class_tc1();
	}

	@Test(dataProvider="post_requestBody")
	//@Test(dataProvider="post_dataProvider",dataProviderClass=testNG_post_dataProvider.class)
	public static void post_Executor(String name, String job) throws IOException {
		post_requestBody ="{\r\n"
				+ "    \"name\": \""+name+"\",\r\n"
				+ "    \"job\": \""+job+"\"\r\n"
				+ "}" ;
		 log_dir = Dir_Handle.create_DirLog("TestCaseDelivery_class");
		String requestBody = RPost_Requset.RPost_request_Tc2();

		String endpoint = EPostRequest_class.EPostRequest_class_tc1();
		for (int i = 0; i < 5; i++) {

			int statuscode = post_statuscode(requestBody, endpoint);
			System.out.println(statuscode);
			if (statuscode == 201) {

				String responseBody = post_responsebody(requestBody, endpoint);
				
				System.out.println(responseBody);
				Api_logs_handle.evidence_creator(log_dir, "TestCaseDelivery_class", endpoint, requestBody,
						responseBody);
				TestCaseDelivery_class.post_validator(requestBody, responseBody);
				break;

			} else {
				System.out.println("Retry the method when status code is not found");
			}
		}

	}

	public static void post_validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expected_date);
	}

	@AfterTest
	public static void Test_teardown() throws IOException {

		Api_logs_handle.evidence_creator(log_dir, "TestCaseDelivery_class", post_requestBody, post_endpoint,
				post_responsebody);	

}
	}

